using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Gameboard;

public static class PerformBootstrap
{
    const string SceneName = "Bootstrap";

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void Execute()
    {
        // traverse the currently loaded scenes
        for (int sceneIndex = 0; sceneIndex < SceneManager.sceneCount; ++sceneIndex)
        {
            var candidate = SceneManager.GetSceneAt(sceneIndex);

            // early out if already loaded
            if (candidate.name == SceneName)
                return;
        }

        // additively load the bootstrap scene
        SceneManager.LoadScene(SceneName, LoadSceneMode.Additive);
    }
}

public class LoadGameboardSDK : MonoBehaviour
{
    [Tooltip("Name of the scene that will be loaded first")]
    public string TitleScene;
    public static LoadGameboardSDK Instance { get; private set; } = null;

    void Awake()
    {
        // check if an instance already exists
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;

        // prevent the data from being unloaded
        DontDestroyOnLoad(gameObject);

        SceneManager.LoadSceneAsync(TitleScene);
    }
}