# GGJ Template

## Getting started

This template is intended to help quick start a Gameboard project for GGJ.
**IMPORTANT**
In order to properly pull the SDK, you will need to setup AWS on your machine and setup the auth tokens. You can find details for how to do that [here](https://external-codelabs.hosting.lastgameboard.com/codelabs/sdk_access-codelab/).

## SDK Documentation

You can find documentation for how to user the Gameboard SDK [here](https://external-codelabs.hosting.lastgameboard.com/).
A good place to start would be the [Gameboard Game Developers - Best Practices](https://external-codelabs.hosting.lastgameboard.com/codelabs/game_devs_best_practices-codelab/) in the Section **Useful information for Unity SDK Development**, which gives an overview of most of the documentation articles and what they detail along with a link to their contents.

## What is included

This project includes 3 scenes to speed up the setup process

1.  **Bootstrap** - Sets up the Gameboard SDK and redirects to the `Title Scene` set on the Load Gameboard SDK script. (Defaults to TitlePage, the next included scene)

2.  **TitlePage** - Includes a [PlayerArmingScreen prefab](https://external-codelabs.hosting.lastgameboard.com/codelabs/sdk_arm_screen_widget-codelabs/) setup to detect when users join the board, and when enough of the set `Min Required Users` set themselves as ready, the Start Button will appear that will redirect to the set `Next Scene`, currently set to `InitialGameScene`. You can customize this scene by changing out the visuals. See the documentation for further detail on how to customize this prefab.

    - To add users to this scene while in Unity, you can use the [Companion Web Connection](https://external-codelabs.hosting.lastgameboard.com/codelabs/play_web_connection-codelab/) or the [User Presence Event Simulator](https://external-codelabs.hosting.lastgameboard.com/codelabs/sdk_user_presence_simulator-codelabs/) or the [MockMultipleDrawers Widget](https://external-codelabs.hosting.lastgameboard.com/codelabs/sdk-mockdrawer-widget-codelabs/).

3.  **InitialGameScene** - this is a currently empty scene that will be navigated to once the minimum number of users ready up on the TitlePage.
